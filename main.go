package main

import (
	_ "gitee.com/y2h/beeqor/routers"
	_ "gitee.com/y2h/beeqor/initial"
	"github.com/astaxie/beego"
)

func main() {
	beego.Run()
}

