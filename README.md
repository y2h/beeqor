# beeqor

#### 项目介绍
使用beego, gorm和qor admin搭建的后台框架
![image](https://gitee.com/y2h/beeqor/tree/master/doc/snapshot.png)
![image](https://gitee.com/y2h/beeqor/tree/master/doc/snapshot1.png)
#### 软件架构
1. beego, github.com/astaxie/beego
2. gorm, github.com/jinzhu/gorm
3. github.com/qor/admin
4. postgresql


#### 安装教程

1. 克隆本项目：git clone https://gitee.com/y2h/beeqor.git
2. 创建数据库，在文件conf/app.conf中正确输入相关配置
3. 正确配置golang运行环境，运行bee run 自动创建表结构，无数据，可自行添加数据进行测试，如果依赖包找不到，按提示安装好
4. 折腾开始

#### 使用说明

1. 我是一只老菜鸟，爱好编程
2. 相互学习
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)