// @APIVersion 1.0.0
// @Title beego Test API
// @Description beego has a very cool tools to autogenerate documents for your API
// @Contact astaxie@gmail.com
// @TermsOfServiceUrl http://beego.me/
// @License Apache 2.0
// @LicenseUrl http://www.apache.org/licenses/LICENSE-2.0.html
package routers

import (
	"gitee.com/y2h/beeqor/controllers"

	"github.com/astaxie/beego"
)

func init() {
	ns := beego.NewNamespace("/v1",

		beego.NSNamespace("/staff",
			beego.NSInclude(
				&controllers.StaffController{},
			),
		),

		beego.NSNamespace("/category",
			beego.NSInclude(
				&controllers.CategoryController{},
			),
		),

		beego.NSNamespace("/country",
			beego.NSInclude(
				&controllers.CountryController{},
			),
		),

		beego.NSNamespace("/actor",
			beego.NSInclude(
				&controllers.ActorController{},
			),
		),

		beego.NSNamespace("/language",
			beego.NSInclude(
				&controllers.LanguageController{},
			),
		),

		beego.NSNamespace("/inventory",
			beego.NSInclude(
				&controllers.InventoryController{},
			),
		),

		beego.NSNamespace("/payment",
			beego.NSInclude(
				&controllers.PaymentController{},
			),
		),

		beego.NSNamespace("/rental",
			beego.NSInclude(
				&controllers.RentalController{},
			),
		),

		beego.NSNamespace("/city",
			beego.NSInclude(
				&controllers.CityController{},
			),
		),

		beego.NSNamespace("/store",
			beego.NSInclude(
				&controllers.StoreController{},
			),
		),

		beego.NSNamespace("/film",
			beego.NSInclude(
				&controllers.FilmController{},
			),
		),

		beego.NSNamespace("/address",
			beego.NSInclude(
				&controllers.AddressController{},
			),
		),

		beego.NSNamespace("/customer",
			beego.NSInclude(
				&controllers.CustomerController{},
			),
		),

		beego.NSNamespace("/qor_admin_setting",
			beego.NSInclude(
				&controllers.QorAdminSettingController{},
			),
		),

		beego.NSNamespace("/qor_admin_settings",
			beego.NSInclude(
				&controllers.QorAdminSettingsController{},
			),
		),

		beego.NSNamespace("/users",
			beego.NSInclude(
				&controllers.UsersController{},
			),
		),
	)
	beego.AddNamespace(ns)
}
