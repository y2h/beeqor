package models

import "time"

type FilmActor struct {
	ActorId    int       `gorm:"column:actor_id"`
	FilmId     int       `gorm:"column:film_id"`
	LastUpdate time.Time `gorm:"column:last_update;type:timestamp without time zone"`
}
