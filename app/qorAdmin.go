package app

import (
	"net/http"

	"gitee.com/y2h/beeqor/models"
	"github.com/astaxie/beego"
	"github.com/jinzhu/gorm"
	"github.com/qor/admin"
)

// QorController operations for qor admin
type QorController struct {
	beego.Controller
}

func BeeQor() {
	//	db, _ := gorm.Open("postgres", "host=localhost port=5432 user=postgres dbname=bqor password=postgres  sslmode=disable")
	//	// 全局禁用表名复数
	//	db.SingularTable(true)
	//	// 启用Logger,显示详细日志
	//	db.LogMode(true)
	//	QorAdmin(db)
}

//qorAdmin控制器
func QorAdmin(db *gorm.DB) {
	adm := admin.New(&admin.AdminConfig{DB: db})
	beego.Info("begin to create table")
	db.AutoMigrate(&models.Staff{})
	db.AutoMigrate(&models.Category{})
	db.AutoMigrate(&models.FilmCategory{})
	db.AutoMigrate(&models.Country{})
	db.AutoMigrate(&models.Actor{})
	db.AutoMigrate(&models.Language{})
	db.AutoMigrate(&models.Inventory{})
	db.AutoMigrate(&models.Payment{})
	db.AutoMigrate(&models.City{})
	db.AutoMigrate(&models.Store{})
	db.AutoMigrate(&models.Film{})
	db.AutoMigrate(&models.Address{})
	db.AutoMigrate(&models.FilmActor{})
	db.AutoMigrate(&models.Customer{})
	db.AutoMigrate(&models.QorAdminSetting{})
	beego.Info("create table end")
	adm.AddResource(&models.Staff{}, &admin.Config{Menu: []string{"后台管理"}})

	adm.AddResource(&models.Category{}, &admin.Config{Menu: []string{"后台管理"}})

	adm.AddResource(&models.FilmCategory{}, &admin.Config{Menu: []string{"后台管理"}})

	adm.AddResource(&models.Country{}, &admin.Config{Menu: []string{"后台管理"}})

	adm.AddResource(&models.Actor{}, &admin.Config{Menu: []string{"后台管理"}})

	adm.AddResource(&models.Language{}, &admin.Config{Menu: []string{"后台管理"}})

	adm.AddResource(&models.Inventory{}, &admin.Config{Menu: []string{"后台管理"}})

	adm.AddResource(&models.Payment{}, &admin.Config{Menu: []string{"后台管理"}})

	adm.AddResource(&models.Rental{}, &admin.Config{Menu: []string{"后台管理"}})

	adm.AddResource(&models.City{}, &admin.Config{Menu: []string{"后台管理"}})

	adm.AddResource(&models.Store{}, &admin.Config{Menu: []string{"后台管理"}})

	adm.AddResource(&models.Film{}, &admin.Config{Menu: []string{"后台管理"}})

	adm.AddResource(&models.Address{}, &admin.Config{Menu: []string{"后台管理"}})

	adm.AddResource(&models.FilmActor{}, &admin.Config{Menu: []string{"后台管理"}})

	adm.AddResource(&models.Customer{}, &admin.Config{Menu: []string{"后台管理"}})

	mux := http.NewServeMux()
	adm.MountTo("/admin", mux)
	beego.Handler("/admin/*", mux)
}
